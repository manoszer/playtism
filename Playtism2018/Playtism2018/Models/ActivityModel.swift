//
//  ActivityModel.swift
//  Playtism2018
//
//  Created by Manolis Zervos on 25/03/2018.
//  Copyright © 2018 Manolis Zervos. All rights reserved.
//

import ObjectMapper

class ActivityModel : Mappable {
    
    var activityId: Int?
    var activityTitle: String?
    var activityTypeName: String?
    var activityLevels: [ActivityLevelModel]?
    
    init() {
        
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        activityId                 <- map["activityId"]
        activityTitle              <- map["activityTitle"]
        activityTypeName           <- map["activityTypeName"]
        activityLevels             <- map["activityLevels"]
    }
}
