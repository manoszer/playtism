//
//  ActivitiesResponse.swift
//  Playtism2018
//
//  Created by Manolis Zervos on 25/03/2018.
//  Copyright © 2018 Manolis Zervos. All rights reserved.
//

import ObjectMapper

class ActivitiesResponse: Mappable {
    var activities: [ActivityModel]?
    
    required init?(map: Map) {
    }
    
    // Mappable
    func mapping(map: Map) {
        activities            <- map["activities"]
    }
}
