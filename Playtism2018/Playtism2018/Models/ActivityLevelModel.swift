//
//  ActivityLevelModel.swift
//  Playtism2018
//
//  Created by Manolis Zervos on 25/03/2018.
//  Copyright © 2018 Manolis Zervos. All rights reserved.
//

import ObjectMapper

class ActivityLevelModel : Mappable {
    
    var title: String?
    var option1: String?
    var option2: String?
    var option3: String?
    var option4: String?
    var solution: Int?
    var didAnsweredCorectlly: Bool?
    
    init() {
    }
    
    convenience required init?(map: Map) {
        self.init()
    }
    
    // Mappable
    func mapping(map: Map) {
        title                 <- map["title"]
        option1               <- map["option1"]
        option2               <- map["option2"]
        option3               <- map["option3"]
        option4               <- map["option4"]
        solution              <- map["solution"]
    }
}
