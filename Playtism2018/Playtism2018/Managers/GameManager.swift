//
//  GameManager.swift
//  Playtism
//
//  Created by Pantelis on 6/18/15.
//  Copyright (c) 2015 zervos. All rights reserved.
//

import UIKit
import ObjectMapper

class game : NSObject {
    var gameType : String
    var gameDescription : String
    var imageName : String
    var buttonFrame : CGRect
    var interactionType : String
    var dragableImage : String
    var initialFrame : CGRect
    var successfulFrame : CGRect
    
    init(_gameType : String, _description : String, _imageName : String, _buttonFrame : CGRect, _interactionType : String, _dragableImage : String, _initialFrame : CGRect, _successfulFrame : CGRect){
        gameType = _gameType
        gameDescription = _description
        imageName = _imageName
        buttonFrame = _buttonFrame
        interactionType = _interactionType
        dragableImage = _dragableImage
        initialFrame = _initialFrame
        successfulFrame = _successfulFrame
        
        super.init()
    }
}

public enum ActivityNameEnum: String {
    case boxes = "boxes"
    case segmented = "segmented"
    case doubleButton = "doubleButton"
    case multiplChoice = "multiplChoice"
    case drawLine = "drawLine"
}

class GameManager: NSObject {
    static let sharedInstance = GameManager()
    
    var currentGameIndex : Int = 0;
    var data : NSArray = [
        game(_gameType: "right_left", _description: "Βρές το αριστερά παγωτό", _imageName: "Slice 7", _buttonFrame: CGRect(x: 30, y: 150, width: 300, height: 400),  _interactionType : "tap", _dragableImage : "", _initialFrame : CGRect.zero, _successfulFrame : CGRect.zero),
         game(_gameType: "right_left", _description: "Βρές το δεξια κοριτσάκι", _imageName:"Slice 1", _buttonFrame: CGRect(x: 500, y: 140, width: 500, height: 600),  _interactionType : "tap", _dragableImage : "", _initialFrame : CGRect.zero, _successfulFrame : CGRect.zero),
        game(_gameType: "right_left", _description: "Βρές το δεξια παγωτό", _imageName: "Slice 7", _buttonFrame: CGRect(x: 680, y: 140, width: 300, height: 410), _interactionType : "tap", _dragableImage : "", _initialFrame : CGRect.zero, _successfulFrame : CGRect.zero),
       game(_gameType: "right_left", _description: "Βρές το αριστερά κοριτσάκι", _imageName: "Slice 1", _buttonFrame: CGRect(x: 10, y: 100, width: 500, height: 600), _interactionType : "tap", _dragableImage : "", _initialFrame : CGRect.zero, _successfulFrame : CGRect.zero),
        game(_gameType: "near_far", _description: "Βρές τη πιο κοντινή βάρκα", _imageName: "Slice 3", _buttonFrame: CGRect(x: 400, y: 200, width: 320, height: 350), _interactionType : "tap", _dragableImage : "", _initialFrame : CGRect.zero, _successfulFrame : CGRect.zero),
        game(_gameType: "near_far", _description: "Βρές το πιο κοντινό αερόστατο", _imageName: "Slice 4", _buttonFrame: CGRect(x: 180 , y: 240, width: 370, height: 470), _interactionType : "tap", _dragableImage : "", _initialFrame : CGRect.zero, _successfulFrame : CGRect.zero),
        game(_gameType: "near_far", _description: "Βρές το πιο μακρινό αερόστατο", _imageName: "Slice 4", _buttonFrame: CGRect(x: 80 , y: 310, width: 70, height: 90), _interactionType : "tap", _dragableImage : "", _initialFrame : CGRect.zero, _successfulFrame : CGRect.zero),
        game(_gameType: "near_far", _description: "Βρες τον πιο κοντινό κύκνο", _imageName: "Slice 12", _buttonFrame: CGRect(x: 160 , y: 500, width: 570, height: 260), _interactionType : "tap", _dragableImage : "", _initialFrame : CGRect.zero, _successfulFrame : CGRect.zero),
        game(_gameType: "little_big", _description: "Βρες το πιο μεγάλο πρόβατο", _imageName: "Slice 11", _buttonFrame: CGRect(x: 230 , y: 80, width: 340 , height: 530), _interactionType : "tap", _dragableImage : "", _initialFrame : CGRect.zero, _successfulFrame : CGRect.zero),
        game(_gameType: "little_big", _description: "Βρές την πιο μικρή μπάλα", _imageName: "Slice 5", _buttonFrame: CGRect(x: 580 , y: 540, width: 200 , height: 200), _interactionType : "tap", _dragableImage : "", _initialFrame : CGRect.zero, _successfulFrame : CGRect.zero),
        game(_gameType: "little_big", _description: "Βρες τον πιο μεγάλο ελέφαντα", _imageName: "Slice 10", _buttonFrame: CGRect(x: 430 , y: 80, width: 590 , height: 650), _interactionType : "tap", _dragableImage : "", _initialFrame : CGRect.zero, _successfulFrame : CGRect.zero),
        game(_gameType: "little_big", _description: "Βρες το πιο μικρό πρόβατο", _imageName: "Slice 11", _buttonFrame: CGRect(x: 580 , y: 320, width: 240 , height: 330), _interactionType : "tap", _dragableImage : "", _initialFrame : CGRect.zero, _successfulFrame : CGRect.zero),
        game(_gameType: "little_big", _description: "Βρές την πιο μεγάλη μπάλα", _imageName: "Slice 5", _buttonFrame: CGRect(x: 40 , y: 10, width: 600 , height: 600), _interactionType : "tap", _dragableImage : "", _initialFrame : CGRect.zero, _successfulFrame : CGRect.zero),
        game(_gameType: "little_big", _description: "Βρες τον πιο μικρό ελέφαντα", _imageName: "Slice 10", _buttonFrame: CGRect(x: 0 , y: 290, width: 500 , height: 400), _interactionType : "tap", _dragableImage : "", _initialFrame : CGRect.zero, _successfulFrame : CGRect.zero),
        game(_gameType: "tall_short", _description: "Βρές το πιο ψηλό παιδάκι", _imageName: "Slice 6", _buttonFrame: CGRect(x: 520, y: 120, width: 250, height: 600), _interactionType : "tap", _dragableImage : "", _initialFrame : CGRect.zero, _successfulFrame : CGRect.zero),
        game(_gameType: "tall_short", _description: "Βρες τον πιο ψηλό ελέφαντα", _imageName: "Slice 10", _buttonFrame: CGRect(x: 430 , y: 80, width: 590 , height: 650), _interactionType : "tap", _dragableImage : "", _initialFrame : CGRect.zero, _successfulFrame : CGRect.zero),
        game(_gameType: "tall_short", _description: "Βρές το πιο κοντό παιδάκι", _imageName: "Slice 6", _buttonFrame: CGRect(x: 320, y: 260, width: 210, height: 420), _interactionType : "tap", _dragableImage : "", _initialFrame : CGRect.zero, _successfulFrame : CGRect.zero),
        game(_gameType: "tall_short", _description: "Βρες το πιο κοντό πρόβατο", _imageName: "Slice 11", _buttonFrame: CGRect(x: 580 , y: 320, width: 240 , height: 330), _interactionType : "tap", _dragableImage : "", _initialFrame : CGRect.zero, _successfulFrame : CGRect.zero),
        game(_gameType: "tall_short", _description: "Βρές το πιο ψηλό κτίριο", _imageName: "Slice 2", _buttonFrame: CGRect(x: 400, y: 80, width: 300, height: 600), _interactionType : "tap", _dragableImage : "", _initialFrame : CGRect.zero, _successfulFrame : CGRect.zero),
        game(_gameType: "tall_short", _description: "Βρες τον πιο κοντό ελέφαντα", _imageName: "Slice 10", _buttonFrame: CGRect(x: 0 , y: 290, width: 500 , height: 400), _interactionType : "tap", _dragableImage : "", _initialFrame : CGRect.zero, _successfulFrame : CGRect.zero),
        game(_gameType: "in_out", _description: "Σύρε τη μπάλα μέσα στο τετράγωνο", _imageName: "Slice 8", _buttonFrame: CGRect(x: 400, y: 80, width: 300, height: 600), _interactionType : "drag", _dragableImage : "football_PNG1091", _initialFrame : CGRect(x: 50, y: 270, width: 220, height: 220), _successfulFrame : CGRect(x: 680, y: 130, width: 300,height: 500)),
        game(_gameType: "in_out", _description: "Σύρε το γλειφιτζόυρι μέσα στο κουτί", _imageName: "Slice 13", _buttonFrame: CGRect(x: 400, y: 80, width: 300, height: 600), _interactionType : "drag", _dragableImage : "candy", _initialFrame : CGRect(x: 80, y: 80, width: 200, height: 200), _successfulFrame : CGRect(x: 220, y: 360, width: 500,height: 200)),
        game(_gameType: "in_out", _description: "Σύρε τη πεταλούδα μέσα στον κόκκινο κύκλο", _imageName: "Slice 9", _buttonFrame: CGRect(x: 400, y: 80, width: 300, height: 600), _interactionType : "drag", _dragableImage : "butterfly_PNG1053", _initialFrame : CGRect(x: 780, y: 580, width: 150, height: 150), _successfulFrame : CGRect(x: 380, y: 230, width: 380,height: 380)),
        game(_gameType: "up_down", _description: "Βρες το πάνω κουτάκι", _imageName: "Slice14", _buttonFrame: CGRect(x: 380, y: 190, width: 240, height: 100), _interactionType : "tap", _dragableImage : "", _initialFrame : CGRect.zero, _successfulFrame : CGRect.zero),
        game(_gameType: "up_down", _description: "Πάτα το βελάκι που δείχνει προς τα κάτω", _imageName: "Slice17", _buttonFrame: CGRect(x: 280, y: 490, width: 220, height: 140), _interactionType : "tap", _dragableImage : "", _initialFrame : CGRect.zero, _successfulFrame : CGRect.zero),
         game(_gameType: "up_down", _description: "Βρές το πάνω βιβλίο", _imageName: "Slice16", _buttonFrame: CGRect(x: 500, y: 300, width: 350, height: 150), _interactionType : "tap", _dragableImage : "", _initialFrame : CGRect.zero, _successfulFrame : CGRect.zero),
         game(_gameType: "up_down", _description: "Πάτα το κάτω κουτάκι", _imageName: "Slice14", _buttonFrame: CGRect(x: 420, y: 620, width: 240, height: 100), _interactionType : "tap", _dragableImage : "", _initialFrame : CGRect.zero, _successfulFrame : CGRect.zero),
        game(_gameType: "up_down", _description: "Πάτα το βελάκι που δείχνει προς τα πάνω", _imageName: "Slice17", _buttonFrame: CGRect(x: 370, y: 370, width: 200, height: 100), _interactionType : "tap", _dragableImage : "", _initialFrame : CGRect.zero, _successfulFrame : CGRect.zero)
       
       
        ]
    
    var currentGameDataArray : NSArray = []
    var currentGame : String = ""
    var currentGameType : String = ""
    
    func changeCurrentGameType(_ gameType : String){
        currentGameType = gameType
        updateGameDataArray(gameType)
    }
    
    func changeCurrentGame(_ game : String){
       currentGame = game
    }
    
    func updateGameDataArray(_ gameType : String){
        let predicate = NSPredicate(format: "gameType == %@", gameType)
        currentGameDataArray = data.filtered(using: predicate) as NSArray
    }
    
    func getNextGame() -> game{
        if (currentGame == "random") {
            let index = arc4random_uniform(UInt32(data.count))
            
            return data[Int(index)] as! game
        }
        
        let nextGame: AnyObject = currentGameDataArray[currentGameIndex] as AnyObject
        
        currentGameIndex = (currentGameIndex + 1) % currentGameDataArray.count

        return nextGame as! game
    }
    
    func reset() {
        currentGame = ""
        currentGameType = ""
        currentGameIndex = 0
    }
    
    var activities: [ActivityModel]?
    
    func getActivity(for name: ActivityNameEnum) -> ActivityModel? {
        return self.activities?.first(where: {$0.activityTypeName == name.rawValue})
    }
    
    func getActivities() {
        if let path = Bundle.main.path(forResource: "playtism", ofType: "json") {
            do {
                let jsonString = try String(contentsOfFile: path, encoding: .utf8)
                if let apiResponse = Mapper<ActivitiesResponse>().map(JSONString: jsonString) {
                    
                    if let activities = apiResponse.activities {
                        self.activities = activities
                        return
                    }
                }
            } catch {
                print("getActivities() error")
            }
        }
    }
}
