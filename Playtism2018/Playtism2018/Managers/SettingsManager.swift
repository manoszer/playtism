//
//  SettingsManager.swift
//  Playtism
//
//  Created by Pantelis on 10/11/15.
//  Copyright © 2015 zervos. All rights reserved.
//

import UIKit

class SettingsManager: NSObject {
    static let sharedInstance = SettingsManager()
    
    func updateColorWith(_ color : UIColor) {
        let colorData : Data = NSKeyedArchiver.archivedData(withRootObject: color)
        let ud : UserDefaults = UserDefaults.standard
        ud.set(colorData, forKey: "bgColor")
        ud.synchronize()
    }
    
    func updatePlayerNameWith(_ name : NSString) {
        let ud : UserDefaults = UserDefaults.standard
        ud.set((" " + (name as String)), forKey: "playerName")
        ud.synchronize()
    }
    
    func getCurrentColor() -> UIColor? {
        let ud : UserDefaults = UserDefaults.standard
        let colorData : Data? = ud.object(forKey: "bgColor") as? Data
        if colorData == nil{
            return ColorPalette.green
        }
        
        return NSKeyedUnarchiver.unarchiveObject(with: colorData!) as? UIColor
    }
 
    func getPlayerName() -> NSString {
        let ud : UserDefaults = UserDefaults.standard
        let name : NSString? = ud.object(forKey: "playerName") as? NSString
       
        if name != nil {
            return ud.object(forKey: "playerName") as! NSString
        }
        return ""
    }
    
}
