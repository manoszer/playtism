//
//  WelcomeViewController.swift
//  Playtism2018
//
//  Created by Manolis Zervos on 24/03/2018.
//  Copyright © 2018 Manolis Zervos. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var playButton: MainButton!
    @IBOutlet weak var tutorialButton: MainButton!
    @IBOutlet weak var backroundView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        // read the activities from the local json and initialize the gamemanager
        GameManager.sharedInstance.getActivities()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        animateView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
        
        playButton.transform = CGAffineTransform(scaleX: 0, y: 0)
        tutorialButton.transform = CGAffineTransform(scaleX: 0, y: 0)
        
        self.backroundView.backgroundColor = SettingsManager.sharedInstance.getCurrentColor()
        
        let playerName : NSString = SettingsManager.sharedInstance.getPlayerName()
        welcomeLabel.text = "Καλωσήρθες" + (playerName as String) + ". Ας παίξουμε!!!"
    }

    // MARK: - UIMethods
    
    private func setupUI() {
        playButton.changeTitle(title: NSLocalizedString("Ας παίξουμε", comment: ""))
        tutorialButton.changeTitle(title: NSLocalizedString("Οδηγίες", comment: ""))
    }
    
    // MARK: - Animation Methods
    
    func animateView () {
        
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
        }, completion: { (Bool) -> Void in
        })
        
        UIView.animate(withDuration: 0.1, animations: { () -> Void in
            
            self.playButton.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            
        }, completion: { (Bool) -> Void in
            
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                
                self.playButton.transform = CGAffineTransform.identity
                self.playButton.layoutSubviews()
            }, completion: { (Bool) -> Void in
                self.playButton.layoutSubviews()
            })
        })
        
        UIView.animate(withDuration: 0.2, delay: 0.2, options: UIViewAnimationOptions.curveEaseIn, animations: { () -> Void in
            
            self.tutorialButton.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            
        }) { (Bool) -> Void in
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                
                self.tutorialButton.transform = CGAffineTransform.identity
                self.tutorialButton.layoutSubviews()
            }, completion: { (Bool) -> Void in
                self.tutorialButton.layoutSubviews()
            })
            
        }
    }
    
    // MARK: - IBActions
    @IBAction func playButtonTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "GamePickerViewController") as! GamePickerViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func tutorialButtonTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "TutorialViewController") as! TutorialViewController
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func settingsButtonTapped(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
        self.present(vc, animated: true, completion: nil)
    }
}
