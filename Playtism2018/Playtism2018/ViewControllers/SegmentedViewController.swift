//
//  SegmentedViewController.swift
//  Playtism2018
//
//  Created by Manolis Zervos on 17/03/2019.
//  Copyright © 2019 Manolis Zervos. All rights reserved.
//

import UIKit

class SegmentedViewController: UIViewController {
    @IBOutlet weak var backroundView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    fileprivate var activity: ActivityModel
    fileprivate var randomTasksArray: [ActivityLevelModel]
    fileprivate var successCount: Int = 0
    fileprivate var failedCount: Int = 0
    
    init(with activity: ActivityModel) {
        self.activity = activity
        self.randomTasksArray = activity.activityLevels!.getRandomElements(count: 10)
        super.init(nibName: "SegmentedViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backroundView.backgroundColor = SettingsManager.sharedInstance.getCurrentColor()
        setupTableView()
        self.titleLabel.text = activity.activityTitle
        updateCounter()
    }
    
    fileprivate func updateCounter() {
         self.counterLabel.text = String(format: "%d/%d", successCount + failedCount, randomTasksArray.count)
    }
    
    private func setupTableView() {
        let nib0 = UINib.init(nibName: "SegmentedControlTableViewCell", bundle: nil)
        self.tableView.register(nib0, forCellReuseIdentifier: "SegmentedControlTableViewCell")
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 44
    }

    fileprivate func completeActivity() {
        let vc = FinalViewController(with: successCount, in: randomTasksArray.count)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - IBActions
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension SegmentedViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return randomTasksArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SegmentedControlTableViewCell", for: indexPath) as! SegmentedControlTableViewCell
        cell.updateCell(with: randomTasksArray[indexPath.row], and: self)
        return cell
    }
}

extension SegmentedViewController: SegmentedControlTableViewCellDelegate {
    func didSelectAnswer() {
        self.successCount = randomTasksArray.filter({$0.didAnsweredCorectlly == true}).count
        self.failedCount = randomTasksArray.filter({$0.didAnsweredCorectlly == false}).count
        self.updateCounter()
        if successCount + failedCount == randomTasksArray.count {
            self.completeActivity()
        }
    }
}
