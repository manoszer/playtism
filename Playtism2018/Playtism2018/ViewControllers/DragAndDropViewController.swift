//
//  DragAndDropViewController.swift
//  Playtism2018
//
//  Created by Manolis Zervos on 17/03/2019.
//  Copyright © 2019 Manolis Zervos. All rights reserved.
//

import UIKit

class DragAndDropViewController: UIViewController {
    @IBOutlet weak var backroundView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var firstBoxImageView: UIImageView!
    @IBOutlet weak var secondBoxImageView: UIImageView!
    
    fileprivate var activity: ActivityModel
    fileprivate var successCount: Int = 0
    
    fileprivate var currentTask: ActivityLevelModel
    fileprivate var currentTaskIndex: Int = 0
    fileprivate var randomTasksArray: [ActivityLevelModel]
    
    var dragableLabel = UILabel()
    var successfulFrame = CGRect.zero
    var failedFrame = CGRect.zero
    
    init(with activity: ActivityModel) {
        self.activity = activity
        self.randomTasksArray = activity.activityLevels!.getRandomElements(count: Int.random(in: 6..<11))
        self.currentTask = randomTasksArray.first!
        super.init(nibName: "DragAndDropViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backroundView.backgroundColor = SettingsManager.sharedInstance.getCurrentColor()
        self.titleLabel.text = activity.activityTitle
        updateCounter()
        setupDragNDropScreen(for: currentTask)
    }
    
    func setupDragNDropScreen(for activityLevel : ActivityLevelModel) {
        dragableLabel = UILabel(frame : CGRect(x: CGFloat.random(in: 60..<200), y: CGFloat.random(in: 120..<200), width: 180, height: 50))
        dragableLabel.isUserInteractionEnabled = true
        dragableLabel.numberOfLines = 0
        dragableLabel.textAlignment = .center
        dragableLabel.text = activityLevel.title
        dragableLabel.alpha = 1
        dragableLabel.font = UIFont(name: "MarkerFelt-Wide", size: 14)
        dragableLabel.textColor = ColorPalette.main
        backroundView.addSubview(dragableLabel)
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.handlePan(_:)))
        dragableLabel.addGestureRecognizer(panGesture)
        
        if activityLevel.solution == 0 {
            successfulFrame = firstBoxImageView.frame
            failedFrame = secondBoxImageView.frame
        } else {
            failedFrame = firstBoxImageView.frame
            successfulFrame = secondBoxImageView.frame
        }
    }
    
    @objc func handlePan(_ sender : UIPanGestureRecognizer) {
        let point = sender.location(in: backroundView)
        
        if sender.state == UIGestureRecognizerState.began {
            
        }
        
        dragableLabel.center = point
        NSLog("point %@", NSStringFromCGPoint(point))
        
        if sender.state == UIGestureRecognizerState.ended {
            if successfulFrame.contains(point) {
                successCount += 1
                UIView.animate(withDuration: 0.5, animations: { () -> Void in
                    self.dragableLabel.frame = CGRect(x: self.dragableLabel.frame.origin.x , y:self.dragableLabel.frame.origin.y, width: 50, height: 50)
                    self.dragableLabel.alpha = 0
                    self.dragableLabel.center = CGPoint(x: self.successfulFrame.origin.x + self.successfulFrame.width/2, y: self.successfulFrame.origin.y + self.successfulFrame.height/2)
                }, completion: { (Bool) -> Void in
                    self.continueToNextLevel()
                })
            } else if failedFrame.contains(point) {
                UIView.animate(withDuration: 0.5, animations: { () -> Void in
                    self.dragableLabel.frame = CGRect(x: self.dragableLabel.frame.origin.x , y:self.dragableLabel.frame.origin.y, width: 50, height: 50)
                    self.dragableLabel.alpha = 0
                    self.dragableLabel.center = CGPoint(x: self.failedFrame.origin.x + self.failedFrame.width/2, y: self.failedFrame.origin.y + self.failedFrame.height/2)
                }, completion: { (Bool) -> Void in
                    self.continueToNextLevel()
                })
            }
        }
    }
    
    fileprivate func updateCounter() {
        self.counterLabel.text = String(format: "%d/%d", currentTaskIndex + 1, randomTasksArray.count)
    }
    
    fileprivate func continueToNextLevel() {
        self.dragableLabel.removeFromSuperview()
        currentTaskIndex += 1
        self.updateCounter()
        if currentTaskIndex == randomTasksArray.count {
            self.completeActivity()
        } else {
            self.currentTask = randomTasksArray[currentTaskIndex]
            self.setupDragNDropScreen(for: currentTask)
        }
    }
    
    fileprivate func completeActivity() {
        let vc = FinalViewController(with: successCount, in: randomTasksArray.count)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - IBActions
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}



