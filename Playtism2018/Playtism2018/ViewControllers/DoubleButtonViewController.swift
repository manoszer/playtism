//
//  DoubleButtonViewController.swift
//  Playtism2018
//
//  Created by Manolis Zervos on 17/03/2019.
//  Copyright © 2019 Manolis Zervos. All rights reserved.
//

import UIKit

class DoubleButtonViewController: UIViewController {
    @IBOutlet weak var backroundView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var activityTitleLabel: UILabel!
    @IBOutlet weak var counterLabel: UILabel!
    @IBOutlet weak var firstButton: MainButton!
    @IBOutlet weak var secondButton: MainButton!
    
    fileprivate var activity: ActivityModel
    fileprivate var currentTask: ActivityLevelModel
    fileprivate var currentTaskIndex: Int = 0
    fileprivate var randomTasksArray: [ActivityLevelModel]
    
    fileprivate var successCount: Int = 0
    
    init(with activity: ActivityModel) {
        self.activity = activity
        self.randomTasksArray = activity.activityLevels!.getRandomElements(count: Int.random(in: 6..<10))
        self.currentTask = self.randomTasksArray.first!
        super.init(nibName: "DoubleButtonViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.prepareUIForActivity()
        self.prepareUiForCurrentTask()
    }
    
    fileprivate func prepareUIForActivity() {
        self.backroundView.backgroundColor = SettingsManager.sharedInstance.getCurrentColor()
        self.titleLabel.text = ""
        self.activityTitleLabel.text = activity.activityTitle
        self.firstButton.tag = 0
        self.secondButton.tag = 1
    }
    
    fileprivate func prepareUiForCurrentTask() {
        self.counterLabel.text = String(format: "%d/%d", currentTaskIndex + 1, randomTasksArray.count)
        self.firstButton.setTitle(currentTask.option1, for: .normal)
        self.secondButton.setTitle(currentTask.option2, for: .normal)
    }
    
    fileprivate func animateButtonSelection(sender: MainButton ,isCorrect: Bool) {
        if isCorrect {
            successCount += 1
        }
        
        UIView.animate(withDuration: 0.6,
                       animations: {
                        sender.transform = CGAffineTransform(scaleX: 0.6, y: 0.6)
                        sender.backgroundColor = isCorrect ? UIColor.green : UIColor.red
        },
                       completion: { _ in
                        UIView.animate(withDuration: 0.2, animations: {
                            sender.transform = CGAffineTransform.identity
                        }, completion: {[weak self] _ in
                            sender.colorUpButton(color: ColorPalette.main)
                            if self?.currentTaskIndex == (self?.randomTasksArray.count)! - 1 {
                                self?.completeActivity()
                            } else {
                                self?.currentTaskIndex += 1
                                self?.currentTask = self!.randomTasksArray[(self?.currentTaskIndex)!]
                                self?.prepareUiForCurrentTask()
                                self?.view.layoutIfNeeded()
                            }
                        })
        })
    }
    
    fileprivate func completeActivity() {
        let vc = FinalViewController(with: successCount, in: randomTasksArray.count)
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    // MARK: - IBActions
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func buttonTapped(_ sender: MainButton) {
        self.animateButtonSelection(sender: sender, isCorrect: sender.tag == currentTask.solution)
    }
}
