//
//  GamePickerViewController.swift
//  Playtism2018
//
//  Created by Manolis Zervos on 25/03/2018.
//  Copyright © 2018 Manolis Zervos. All rights reserved.
//

import UIKit

class GamePickerViewController: UIViewController {
    @IBOutlet weak var firstButton: MainButton!
    @IBOutlet weak var secondButton: MainButton!
    @IBOutlet weak var thirdButton: MainButton!
    @IBOutlet weak var fourthButton: MainButton!
    @IBOutlet weak var fifthButton: MainButton!
    @IBOutlet weak var backroundView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.backroundView.backgroundColor = SettingsManager.sharedInstance.getCurrentColor()
        initializeButtonPositions()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        animateView()
    }

    // MARK: - UIMethods
    private func setupUI() {
        firstButton.changeTitle(title: NSLocalizedString("Πρώτη δραστηριότητα", comment: ""))
        secondButton.changeTitle(title: NSLocalizedString("Δεύτερη δραστηριότητα", comment: ""))
        thirdButton.changeTitle(title: NSLocalizedString("Τρίτη δραστηριότητα", comment: ""))
        fourthButton.changeTitle(title: NSLocalizedString("Τέταρτη δραστηριότητα", comment: ""))
        fifthButton.changeTitle(title: NSLocalizedString("Στην τύχη", comment: ""))
    }
    // MARK: - Animation methods
    private func initializeButtonPositions() {
        firstButton.transform = CGAffineTransform(scaleX: 0, y: 0)
        secondButton.transform = CGAffineTransform(scaleX: 0, y: 0)
        thirdButton.transform = CGAffineTransform(scaleX: 0, y: 0)
        fourthButton.transform = CGAffineTransform(scaleX: 0, y: 0)
        fifthButton.transform = CGAffineTransform(scaleX: 0, y: 0)
    }
    
    func animateView () {
        UIView.animate(withDuration: 0.2, delay: 0.0, options: UIViewAnimationOptions(), animations: { () -> Void in
            self.firstButton.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            self.firstButton.layoutSubviews()
        }) { (Bool) -> Void in
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.firstButton.transform = CGAffineTransform.identity
                self.firstButton.layoutSubviews()
            }, completion: { (Bool) -> Void in
            })
        }
        UIView.animate(withDuration: 0.2, delay: 0.1, options: UIViewAnimationOptions(), animations: { () -> Void in
            self.secondButton.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            self.secondButton.layoutSubviews()
        }) { (Bool) -> Void in
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                
                self.secondButton.transform = CGAffineTransform.identity
                self.secondButton.layoutSubviews()
            }, completion: { (Bool) -> Void in
            })
        }
        UIView.animate(withDuration: 0.2, delay: 0.2, options: UIViewAnimationOptions(), animations: { () -> Void in
            self.thirdButton.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            self.thirdButton.layoutSubviews()
        }) { (Bool) -> Void in
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.thirdButton.transform = CGAffineTransform.identity
                self.thirdButton.layoutSubviews()
            }, completion: { (Bool) -> Void in
            })
        }
        UIView.animate(withDuration: 0.2, delay: 0.3, options: UIViewAnimationOptions(), animations: { () -> Void in
            self.fourthButton.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            self.fourthButton.layoutSubviews()
        }) { (Bool) -> Void in
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.fourthButton.transform = CGAffineTransform.identity
                self.fourthButton.layoutSubviews()
            }, completion: { (Bool) -> Void in
            })
        }
        UIView.animate(withDuration: 0.2, delay: 0.4, options: UIViewAnimationOptions(), animations: { () -> Void in
            self.fifthButton.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            self.fifthButton.layoutSubviews()
        }) { (Bool) -> Void in
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.fifthButton.transform = CGAffineTransform.identity
                self.fifthButton.layoutSubviews()
            }, completion: { (Bool) -> Void in
            })
        }
    }
    
    // MARK: - IBActions
    @IBAction func closeButtonTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func gameButtonTapped(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            if let activity = GameManager.sharedInstance.getActivity(for: ActivityNameEnum.doubleButton) {
                let vc = DoubleButtonViewController(with: activity )
                self.navigationController?.pushViewController(vc, animated: true)
            }
        case 1:
            if let activity = GameManager.sharedInstance.getActivity(for: ActivityNameEnum.segmented) {
                let vc = SegmentedViewController(with: activity)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        case 2:
            if let activity = GameManager.sharedInstance.getActivity(for: ActivityNameEnum.boxes) {
                let vc = DragAndDropViewController(with: activity)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        case 3:
            if let activity = GameManager.sharedInstance.getActivity(for: ActivityNameEnum.multiplChoice) {
                let vc = MultipleChoiceViewController(with: activity)
                self.navigationController?.pushViewController(vc, animated: true)
            }
        case 4:
            let sender = UIButton()
            sender.tag = Int.random(in: 0..<4)
            self.gameButtonTapped(sender)
        default:
            break
        }
    }
}
