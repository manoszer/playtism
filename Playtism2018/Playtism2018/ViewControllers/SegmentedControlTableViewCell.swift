//
//  SegmentedControlTableViewCell.swift
//  Playtism2018
//
//  Created by Manolis Zervos on 17/03/2019.
//  Copyright © 2019 Manolis Zervos. All rights reserved.
//

import UIKit

protocol SegmentedControlTableViewCellDelegate: class {
    func didSelectAnswer()
}

class SegmentedControlTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var firstButton: UIButton!
    @IBOutlet weak var secondButton: UIButton!
    
    fileprivate weak var delegate: SegmentedControlTableViewCellDelegate?
    fileprivate var activity: ActivityLevelModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        self.firstButton.setImage(UIImage(named: "icon-checkbox-unselected"), for: .normal)
        self.firstButton.setImage(UIImage(named: "icon-checkbox-selected"), for: .selected)
        self.secondButton.setImage(UIImage(named: "icon-checkbox-unselected"), for: .normal)
        self.secondButton.setImage(UIImage(named: "icon-checkbox-selected"), for: .selected)
    }
    
    func updateCell(with activity: ActivityLevelModel, and delegate: SegmentedControlTableViewCellDelegate) {
        self.delegate = delegate
        self.activity = activity
        self.titleLabel.text = activity.title
    }
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        firstButton.isSelected = sender.tag == 0
        secondButton.isSelected = sender.tag == 1
        activity?.didAnsweredCorectlly = (activity?.solution == sender.tag)
        delegate?.didSelectAnswer()
    }
}
