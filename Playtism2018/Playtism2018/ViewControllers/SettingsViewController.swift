//
//  SettingsViewController.swift
//  Playtism2018
//
//  Created by Manolis Zervos on 24/03/2018.
//  Copyright © 2018 Manolis Zervos. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController, UITextFieldDelegate {
    @IBOutlet weak var textfield: UITextField!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var backroundVIew: UIView!
    @IBOutlet weak var contentViewConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
        setupKeyboardNotifications()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.backroundVIew.backgroundColor = SettingsManager.sharedInstance.getCurrentColor()
        
        textfield.text = SettingsManager.sharedInstance.getPlayerName() as String
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - UI methods
    
    private func setupUI() {
        self.slider.value = ColorPalette().valueForColor(SettingsManager.sharedInstance.getCurrentColor()!)
        self.textfield.delegate = self
    }
    
    // MARK: - keyboard mehtods
    
    private func setupKeyboardNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(SettingsViewController.keboardWillBeVisible(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(SettingsViewController.keboardWillBeDismissed(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    @objc private func keboardWillBeVisible(_ notification : Notification) {
        var info = (notification as NSNotification).userInfo!
        let keyboardFrame: CGRect = (info[UIKeyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardHeight : CGFloat = keyboardFrame.size.height
        
        contentViewConstraint.constant = -keyboardHeight/2.0
        
        UIView.animate(withDuration: 0.2, animations: { () -> Void in
            self.view.layoutIfNeeded()
        }, completion: { (Bool) -> Void in
            
        })
        
    }
    
    @objc private func keboardWillBeDismissed(_ notification : Notification) {
        contentViewConstraint.constant = 0
        
        UIView .animate(withDuration: 0.2, animations: { () -> Void in
            self.view.layoutIfNeeded()
        }, completion: { (Bool) -> Void in
            
        })
    }
    
    @IBAction func didTapOutside(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    // MARK: - IBActions
    @IBAction func sliderValueChanged(_ sender: Any) {
        let color = ColorPalette().colorForValue(slider.value)
        self.backroundVIew.backgroundColor = color
        SettingsManager.sharedInstance.updateColorWith(color)
    }
    
    @IBAction func textfieldEditingChanged(_ sender: Any) {
        let name : NSString = NSString(string: textfield.text!)
        SettingsManager.sharedInstance.updatePlayerNameWith(name)
    }
    
    @IBAction func xButtonTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
