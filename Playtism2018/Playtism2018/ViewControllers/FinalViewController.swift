//
//  FinalViewController.swift
//  Playtism2018
//
//  Created by Manolis Zervos on 17/03/2019.
//  Copyright © 2019 Manolis Zervos. All rights reserved.
//

import UIKit

class FinalViewController: UIViewController {
    @IBOutlet weak var backroundView: UIView!
    @IBOutlet weak var gifImageView: UIImageView!
    @IBOutlet weak var successLabel: UILabel!
    @IBOutlet weak var playButton: MainButton!
    
    fileprivate var gifsArray = ["https://media.giphy.com/media/c1R3XcUXVWAFy/200w_d.gif","https://media.giphy.com/media/peAFQfg7Ol6IE/giphy.gif","https://media.giphy.com/media/j1bTUuCmIlZ4Y/giphy-downsized.gif","https://media.giphy.com/media/l1J3DaHzWEp2bTpYs/giphy.gif"]
    
    fileprivate var success: Int
    fileprivate var total: Int
    
    init(with success: Int, in total: Int) {
        self.success = success
        self.total = total
        super.init(nibName: "FinalViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpUI()
    }
    
    fileprivate func setUpUI() {
        self.backroundView.backgroundColor = SettingsManager.sharedInstance.getCurrentColor()
        let gifURL = gifsArray.randomElement()
        let image = UIImage.gifImageWithURL(gifUrl: gifURL!)
        self.gifImageView.image = image
        
        self.playButton.setTitle("Παίξε ξανά", for: .normal)
        if success == total {
            self.successLabel.text = "Συγχαρητήρια!\n\n Όλες σου οι απαντήσεις ήταν σωστές.\n Ξαναπροσπάθησε για να κρατήσεις το σκορ σου"
        } else if success == total - 1 || success ==  total - 2 {
            self.successLabel.text = String(format: "Μπράβο\n\nΤα πήγες πάρα πολύ καλά. Είχες συνολικά %d από τις %d απαντήσεις σωστές. Ξαναπροσπάθησε για να πετύχεις το τέλειο σκορ", success, total)
        } else if success < 3 {
            self.successLabel.text = "Δεν τα πήγες και τόσο καλά, αλλά μην απογοητεύεσαι. Ξαναπροσπάθησε για να βελτιώσεις το σκορ σου"
        } else {
            self.successLabel.text = String(format: "Μπράβο\n\nΤα πήγες αρκετά καλά. Είχες συνολικά %d από τις %d απαντήσεις σωστές. Ξαναπροσπάθησε για να πετύχεις καλύτερο σκορ", success, total)
        }
    }
    
    @IBAction func playButtonTapped(_ sender: Any) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: GamePickerViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
}
