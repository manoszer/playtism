//
//  MainButton.swift
//  Playtism2018
//
//  Created by Manolis Zervos on 24/03/2018.
//  Copyright © 2018 Manolis Zervos. All rights reserved.
//

import UIKit

class MainButton: UIButton {
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        createButtonUI()
    }
    
    required override init(frame: CGRect) {
        super.init(frame: frame)
        createButtonUI()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.layer.cornerRadius = self.frame.size.height / 2
        self.addShadow()
    }
    
    func createButtonUI() {
        self.setTitleColor(UIColor.white, for: .normal)
        self.titleLabel?.font = UIFont(name: "Marker Felt-Thin", size: 16)
        self.titleLabel?.adjustsFontSizeToFitWidth = true
        self.backgroundColor = ColorPalette.main
    }
    
    func addShadow() {
        self.layer.shadowColor = ColorPalette.backroundlightGray.cgColor
        self.layer.shadowOffset = CGSize.zero
        self.layer.shadowRadius = 5
        self.layer.shadowOpacity = 1
    }
    
    func colorUpButton (color: UIColor) {
        self.backgroundColor = color
    }
    
    func colorUpText (color: UIColor) {
        self.setTitleColor(color, for: .normal)
    }
    
    func changeTitle (title: String) {
        self.setTitle(title, for: .normal)
    }
}

