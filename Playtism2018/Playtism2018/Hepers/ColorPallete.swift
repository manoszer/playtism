//
//  ColorPallete.swift
//  Playtism2018
//
//  Created by Manolis Zervos on 24/03/2018.
//  Copyright © 2018 Manolis Zervos. All rights reserved.
//

import UIKit

struct ColorPalette {
    static let white = UIColor(red: 255/255.0, green: 255/255.0, blue: 255/255.0, alpha: 1.0)  // #C3C3C3
    static let placeHolderGray = UIColor(red: 151/255.0, green: 151/255.0, blue: 151/255.0, alpha: 1.0)  // #979797
    static let blue = UIColor(red: 72/255.0, green: 111/255.0, blue: 255/255.0, alpha: 1.0)  // #486FFF
    static let green = UIColor(red: 45/255.0, green: 209/255.0, blue: 172/255.0, alpha: 0.2)  // #2DD1AC
    static let backroundBlue = UIColor(red: 72/255.0, green: 111/255.0, blue: 255/255.0, alpha: 0.2)
    static let backroundlightGray = UIColor(red: 195/255.0, green: 195/255.0, blue: 195/255.0, alpha: 0.5)
    
    static let main: UIColor = UIColor(red: 61.0/255.0, green: 82.0/255.0, blue: 101.0/255.0, alpha: 1.0)
    
    func colorForValue(_ value : Float) -> UIColor {
        if value < 0.1 {
            return ColorPalette.green
        }
        if value < 0.2 {
            return UIColor.green.withAlphaComponent(0.2)
        }
        if value < 0.3 {
            return UIColor.blue.withAlphaComponent(0.2)
        }
        if value < 0.4 {
            return ColorPalette.backroundBlue
        }
        if value < 0.5 {
            return UIColor.orange.withAlphaComponent(0.2)
        }
        if value < 0.6 {
            return UIColor.magenta.withAlphaComponent(0.2)
        }
        if value < 0.7 {
            return UIColor.purple.withAlphaComponent(0.2)
        }
        if value < 0.8 {
            return UIColor.cyan.withAlphaComponent(0.2)
        }
        if value < 0.9 {
            return ColorPalette.backroundlightGray
        }
        return UIColor.brown.withAlphaComponent(0.2)
    }
    
    func valueForColor(_ color : UIColor) -> Float {
        switch color {
        case ColorPalette.green:
            return 0.0
            
        case UIColor.green.withAlphaComponent(0.2):
            return 0.1
            
        case UIColor.blue.withAlphaComponent(0.2):
            return 0.2
            
        case ColorPalette.backroundBlue:
            return 0.3
            
        case UIColor.orange.withAlphaComponent(0.2):
            return 0.4
            
        case UIColor.magenta.withAlphaComponent(0.2):
            return 0.5
            
        case UIColor.purple.withAlphaComponent(0.2):
            return 0.6
            
        case UIColor.cyan.withAlphaComponent(0.2):
            return 0.7
            
        case ColorPalette.backroundlightGray :
            return 0.8
        default :
            return 0.9
        }
        
    }
}
